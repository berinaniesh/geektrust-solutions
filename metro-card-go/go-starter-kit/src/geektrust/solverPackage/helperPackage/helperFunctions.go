package helperPackage

import "fmt"
import "sort"
import "geektrust/modelPackage"


// Initially it started as a pair, but then I had to include the count of the person type.
type Pair struct {
  Person string // key
  Revenue int // value
  Count int
}

type ByIncome []Pair

func (b ByIncome) Len() int {
  return len(b)
}

func (s ByIncome) Swap(i, j int) {
  s[i], s[j] = s[j], s[i]
}

// Implementing Less() for the sort function
func (s ByIncome) Less(i, j int) bool {
  return s[i].Count > s[j].Count
}

// Printing should be in the order of highest collection etc.
func Print_Data(data modelPackage.Station_Data) {
  fmt.Printf("TOTAL_COLLECTION %v %v %v\n", data.Station_Name, data.Collection, data.Discount)
  fmt.Printf("PASSENGER_TYPE_SUMMARY\n")

  arr := []Pair{{"ADULT", data.Adult_Revenue, data.Adult}, {"SENIOR_CITIZEN", data.Senior_Revenue, data.Senior_citizen}, {"KID", data.Kid_Revenue, data.Kid}}

  sort.Sort(ByIncome(arr))

  if arr[1].Count == arr[2].Count {
    arr[1], arr[2] = arr[2], arr[1]
  }

  for _, val := range(arr) {
    if val.Count != 0 {
    fmt.Printf("%v %v\n", val.Person, val.Count)
  }
  }
}
