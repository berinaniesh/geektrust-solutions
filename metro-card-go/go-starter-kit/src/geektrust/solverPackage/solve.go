package solverPackage

import "geektrust/modelPackage"
import . "geektrust/solverPackage/helperPackage"

// for each job in job_sheet, solve with updating the balance_sheet

func Solve(job_sheet []modelPackage.Journey, balance_sheet map[string]int) {
	Central := modelPackage.Station_Data{"CENTRAL", 0, 0, 0, 0, 0, 0, 0, 0}
	Airport := modelPackage.Station_Data{"AIRPORT", 0, 0, 0, 0, 0, 0, 0, 0}

	for _, val := range job_sheet {
		if val.From == "CENTRAL" {
			Central.Process_Transaction(val, &balance_sheet)
		} else {
			Airport.Process_Transaction(val, &balance_sheet)
		}
	}

	Print_Data(Central)
	Print_Data(Airport)
}
