package modelPackage

// Using package level variable can lead to race conditions and other bugs
// But this seemed the easiest solution here and so using it.
// This will be reimplemented using a singleton or passing the map by reference throughout
// (or something) in the future
var discount_eligible = make(map[string]bool)

func (station *Station_Data) Process_Transaction(journey Journey, balance_sheet *map[string]int) {
  current_bal := (*balance_sheet)[journey.MC]
  money_req := 0
  // Fight magic numbers
  adult_money_req := 200
  senior_money_req := 100
  kid_money_req := 50

// Find Fare
  if journey.Person == "ADULT" {
    money_req = adult_money_req
    station.Adult += 1
  } else if journey.Person == "SENIOR_CITIZEN" {
    money_req = senior_money_req
    station.Senior_citizen += 1
  } else {
    money_req = kid_money_req
    station.Kid += 1
  }

// Check discount
  discount, ok := discount_eligible[journey.MC]
  if ok == false {
    discount_eligible[journey.MC] = true
  } else {
    if discount {
      station.Discount += int(money_req/2)
      money_req = money_req / 2
      discount_eligible[journey.MC] = false
    } else {
      discount_eligible[journey.MC] = true
    }
  }
// Update balance sheets
  if money_req <= current_bal {
    station.Collection += money_req
    (*balance_sheet)[journey.MC] -= money_req
  } else {
    station.Collection += int((money_req - current_bal)*2/100)
    station.Collection += money_req
    (*balance_sheet)[journey.MC] = 0
  }

// Update each category revenue for sorting at last.
  if journey.Person == "ADULT" {
    station.Adult_Revenue += money_req
  } else if journey.Person == "SENIOR_CITIZEN" {
    station.Senior_Revenue += money_req
  } else {
    station.Kid_Revenue += money_req
  }
}
