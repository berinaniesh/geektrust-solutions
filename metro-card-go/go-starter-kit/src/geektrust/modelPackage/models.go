package modelPackage

// Data modelling (Methods in functions.go file)

type Journey struct {
	MC     string // Metro Card
	Person string
	From   string
}

type Station_Data struct {
	Station_Name string
	Collection int
	Discount int
	Adult int
	Kid int
	Senior_citizen int
	Adult_Revenue int
	Senior_Revenue int
	Kid_Revenue int
}
