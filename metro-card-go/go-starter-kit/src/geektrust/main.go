package main

import (
	"bufio"
	"fmt"
	"geektrust/solverPackage"
	"geektrust/modelPackage"
	"os"
	"strconv"
	"strings"
)


func main() {
	cliArgs := os.Args[1:]

	if len(cliArgs) == 0 {
		fmt.Println("Please provide the input file path")
		return
	}

	filePath := cliArgs[0]
	file, err := os.Open(filePath)

	if err != nil {
		fmt.Println("Error opening the input file")
		return
	}

	defer file.Close()

	// Feed data into balance sheet and job sheet
	scanner := bufio.NewScanner(file)
	balance_sheet := make(map[string]int)
	job_sheet := []modelPackage.Journey{}
	for scanner.Scan() {
		line := scanner.Text()
		words := strings.Fields(line)
		if words[0] == "BALANCE" {
			balance_sheet[words[1]], _ = strconv.Atoi(words[2])
		} else if words[0] == "CHECK_IN" {
			tmp_var := modelPackage.Journey{words[1], words[2], words[3]}
			job_sheet = append(job_sheet, tmp_var)
		} else {
			solverPackage.Solve(job_sheet, balance_sheet)
		}
	}
}
