def calc_direction_to_go(source, destination):
    # return the directions to move in human readable string tuple, eg:("E", "N") says to move north-east. 
    x_dir = ""
    y_dir = ""

    if destination.x > source.x:
        x_dir = "E"
    elif destination.x < source.x:
        x_dir = "W"
    else:
        x_dir = "0"

    if destination.y > source.y:
        y_dir = "N"
    elif destination.y < source.y:
        y_dir = "S"
    else:
        y_dir = "0"

    return (x_dir, y_dir)


