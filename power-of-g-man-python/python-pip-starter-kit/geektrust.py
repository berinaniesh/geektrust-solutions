from sys import argv
from helper1 import calc_dir
from helper2 import calc_direction_to_go

# The AI says to model data using classes, changing the code to classes indeed makes it more readable. 
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def traverse(self, tx, ty):
        self.x = tx
        self.y = ty

def calc_dist(source, destination):
    # return (|x2-x1| + |y2-y1|)
    x_dist = abs(source.x - destination.x)
    y_dist = abs(source.y - destination.y)
    return x_dist + y_dist

def main():
    # Source and Destination coordinates in the form of (x,y)
    source = Point(0,0)
    destination = Point(0,0)
    direction = ""

    # Read file
    file = open(argv[1], "r")
    lines = file.readlines()

    # Getting source coordinates
    words = lines[0].split()
    source.x, source.y = int(words[1]), int(words[2])
    direction = words[3]

    # Getting destination coordinates
    words = lines[1].split()
    destination.x, destination.y = int(words[1]), int(words[2])

    # Find which direction to move to get to destination
    direction_to_go = calc_direction_to_go(source, destination)
    # Find number of turns needed
    dir_score = calc_dir(direction, direction_to_go) * 5
    # Find distance
    dist_score = calc_dist(source, destination) * 10

    # This code can be concised by encoding the direction in the distance (without abs() and returning a tuple but this approach seemed more readable). 

    # Print final solution
    print(f"POWER {200-dist_score-dir_score}")

    
if __name__ == "__main__":
    main()
