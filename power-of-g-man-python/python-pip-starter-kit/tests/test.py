import unittest
import os
import subprocess

in_files = [name for name in os.listdir("sample_input")]
out_files = [name for name in os.listdir("sample_output")]

in_files.sort()
out_files.sort()

class Tester(unittest.TestCase):
    def test_number_of_files(self):
        # This test assets if the number of input files and number of output files are equal
        self.assertEqual(len(in_files), len(out_files))

    def test_solutions(self):
        # Check sample_output/output*.txt with the stdout of when each input file is given
        for a,b in zip(in_files, out_files):
            file = open("sample_output/" + b)
            line = file.read()
            file.close()
            run = subprocess.run(["python", "-m", "geektrust", "sample_input/" + a], text=True, capture_output=True)
            self.assertEqual(run.stdout, line)
            
