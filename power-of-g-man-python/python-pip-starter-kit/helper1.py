one = 2-1
two = 3-1
def calc_dir(current_direction, direction_to_go):
    # The "AI" says don't use magic numbers. Sorry!
    one = 2-1
    two = 3-1
    # Match statement not used because the requirements were to code with python version upto 3.9 and match is introduced in python in 3.10

    # We need max 2 turns in any case.
    if (direction_to_go[0] == "0" and direction_to_go[1] == "0"):
        return 0 
    elif (direction_to_go[0] == "0" and direction_to_go[1] != "0"):
        if current_direction == "E" or current_direction == "W":
            return one
        elif current_direction == direction_to_go[1]:
            return 0
        else: 
            return two
    elif (direction_to_go[0] != "0" and direction_to_go[1] == "0"):
        if current_direction == "N" or current_direction == "S":
            return one
        elif current_direction == direction_to_go[0]:
            return 0
        else: 
            return two
    else:
        if current_direction == direction_to_go[0] or current_direction == direction_to_go[1]:
            return one
        else:
            return two
    
